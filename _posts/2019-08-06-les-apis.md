---
layout: post
title:  "Les APIs"
date:   2019-08-06 16:24 +0200
tags: Veille
author: Timothée
background : '/img/bg-index.jpg'
---

C'est quoi une API ?
---------------------

L'acronyme API veut dire "Application Programming Interface", ce qui se traduit en français pas "Interface de Programmation Applicative".

Le principe d'une API, c'est de fournir une passerelle entre différents logiciels et applications pour qu'ils puissent communiquer entre eux. On parle alors de programmes consommateurs et de programmes fournisseurs.

L'intérêt pour les développeurs, c'est de pouvoir utiliser des bouts de fonctionnalités d'autres applications sans avoir à réinveter la roue.

Pour quels cas d'utilisation ?
-------------------------------

Il existe une multitude d'APIs qui servent à faire de nombreuses choses :

- Les APIs pour les graphismes en 3D : au lieu de contrôler directement la carte graphique de l'ordinateur, ce qui serait vite fastidieux, on passe par une surcouche logicielle comme OpenGL, DirectX ou Vulkan pour communiquer avec
- Les APIs de systèmes d'exploitations : les OS permettent aux développeurs d'applications d'accéder à un grand nombre de fonctionnalités comme l'accès au réseau ou la conception d'interfaces graphiques
- Les APIs de services Web : de nombreux sites mettent à disposition des données et des fonctionnalités qui vont ensuite pouvoir être utilisées sur d'autres sites
- Etc...

Quelques exemples d'APIs
---------------------------

### Les APIs tierces

- **Discord** : l'API Discord comprend Rich Presence, qui permet aux développeurs de logiciels (notamment de jeux vidéo) d'afficher des informations sur leur programme dans le profil de l'utilisateur. Spotify en fait usage pour afficher la musique que vous êtes en train d'écouter
	- [Discord pour les développeurs](https://discordapp.com/developers/docs/intro)
- **Twitter** : le réseau social propose une collection d'APIs pour, par exemple, accéder aux données d'un tweet et l'insérer sur son propre site
	- [Twitter pour les développeurs](https://developer.twitter.com/)
- **YouTube** : il est possible d'intégrer des vidéos YouTube sur un site tiers
	- [YouTube pour les développeurs](https://developers.google.com/youtube/)

Il en existe bien d'autres.  
\> [APIs Tierces - MDN](https://developer.mozilla.org/fr/docs/Apprendre/JavaScript/Client-side_web_APIs/Third_party_APIs)

### Les APIs du navigateur

De nos jours, les navigateurs proposent des APIs très intéressantes pour interagir avec un utilisateur :

- **Manipulation du DOM** : on l'a déjà vu en programmant en Javascript, on peut manipuler une page HTML grâce à l'API DOM
```js
let img = document.getElementById("img");
img.setAttribute("src", "./media/panda.png");
```
	- [Introduction au DOM - MDN](https://developer.mozilla.org/fr/docs/Web/API/Document_Object_Model/Introduction)
- **Récupération de données** : on l'a vu en travaillant avec JSON, XMLHttpRequest et Fetch sont des APIs qui nous donnent la possibilité d'accéder à des données stockées dans des fichiers séparés et de les mettre à jour sans recharger la page
```js
const request = new XMLHttpRequest();
request.open("GET", "./fichier.json");
request.responseType = "json";
request.send();
```
	- [XMLHttpRequest - MDN](https://developer.mozilla.org/fr/docs/Web/API/XMLHttpRequest)
- **Géolocalisation** : permet d'accéder aux données GPS d'une machine (notamment les smartphones)
```js
navigator.geolocation.getCurrentPosition();
```
	- [Utiliser la Géolocalisation - MDN](https://developer.mozilla.org/fr/docs/Using_geolocation)
- **WebRTC** : on s'en sert pour accéder à la caméra et au micro de l'ordinateur, très utile pour les webapps qui permettent la visio-conférence comme Discord ou Skype
	- [Démo de l'utilisation de la webcam sur GitHub](https://webrtc.github.io/samples/src/content/getusermedia/gum/)
	- [WebRTC - MDN](https://developer.mozilla.org/fr/docs/Web/Guide/API/WebRTC)
- **Canvas et WebGL** : gèrent respectivement le dessin d'éléments 2D et 3D, utile pour faire des jeux vidéo
	- [WebGL Water](http://madebyevan.com/webgl-water/)
	- [Chrome Experiments](https://experiments.withgoogle.com/collection/chrome)
	- [BananaBread, le FPS dans le navigateur par Mozilla](https://kripken.github.io/misc-js-benchmarks/banana/index.html)
	- [Tutoriel Canvas - MDN](https://developer.mozilla.org/fr/docs/Tutoriel_canvas)
	- [Getting started with WebGL - MDN (en)](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Getting_started_with_WebGL)
- Etc...

Sources :
----------

- [Interface de programmation - Wikipédia](https://fr.wikipedia.org/wiki/Interface_de_programmation)
- [(Vidéo) What is an API? (Application Programming Interface) - Techquickie](https://www.youtube.com/watch?v=6STSHbdXQWI)
- [Introduction aux API du Web - MDN](https://developer.mozilla.org/fr/docs/Apprendre/JavaScript/Client-side_web_APIs/Introduction)
- [C'est quoi une API ? Et pourquoi sont-elles aussi importantes ? - La Fabrique du Net](https://www.lafabriquedunet.fr/blog/definition-api/)
